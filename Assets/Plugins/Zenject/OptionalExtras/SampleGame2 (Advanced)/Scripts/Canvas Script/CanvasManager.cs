﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject.SpaceFighter;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject spaceBackground;
    [SerializeField] private GameObject canvas;

    [SerializeField] private PlayerGui healthDisplay;

    private EnemyView[] enemyArray;

    public void CanvasOn()
    {
        canvas.SetActive(true);
        player.SetActive(false);
        spaceBackground.SetActive(false);
    }

    private void CanvasOff()
    {
        canvas.SetActive(false);
        player.SetActive(true);
        spaceBackground.SetActive(true);
    }

    public void Continue()
    {
        Time.timeScale = 1;

        CanvasOff();

        for (int i = 0; i < enemyArray.Length; i++)
        {
            enemyArray[i].gameObject.SetActive(true);
        }
    }

    public void Retry()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (healthDisplay._player.Health <= 0)
        {
            healthDisplay._player.Health = 100f;

            CanvasOn();

            Time.timeScale = 0;

            enemyArray = FindObjectsOfType<EnemyView>();

            for (int i = 0; i < enemyArray.Length; i++)
            {
                enemyArray[i].gameObject.SetActive(false);
            }
        }
    }
}
